# CCLA User Specification

In order to facilitate development and adoption of applications and software libraries that can be used across multiple institutions in CCLA, the following document
specifies how user information should be structured and made available by each CCLA library.  Each library is responsible for determining how to retrieve and format
user information in a manner consistent both with this standard and with their parent institution's information policies, available toolset, and context.

## Schema

### CclaUser

This is the root document object for a CCLA user.

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| identifier | string | The unique identifier accross ALL CCLA schools (institution:username). |
| memberOf | Institution | The CCLA institution to which this person belongs (BYU, LDSBC, etc). |
| userId | string | A unique identifier for this person at the institution. |
| username | string | The username used by this person to login to institional websites. |
| name | string | The displayable name of the person, constructed by concatenating the `preferredName` with the `familyName/surname` or vice versa according to name order ("Donald Duck"). |
| familyName/surname | string | The official family name of the person ("Duck"). |
| familyNamePosition/surnamePosition | string | Whether the family or surname of this user is "first" (Eastern name order) or "last" (Western name order). |
| givenName | string | The official first and/or middle names of the person ("Donald Fauntleroy"). |
| suffix | string | The official suffix (Jr., Sr., III, etc.) of the person. |
| preferredName | string | The preferred name or nickname by which this person is normally addressed ("Donald"). |
| personalContact | ContactPoint | The personal contact information for this person. |
| workContact | ContactPoint | The professional/work contact information for this person. |
| roles | string[] | The list of roles or memberships held by this person. |

### ContactPoint

Contact information is defined as a nested object that uses a standard structure.  This guarantees that both personal and work contact information can be parsed and understood using the same logic.

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| address | string | The street or mailing address for this contact point. |
| email | string | The email address for this contact point. |
| phone | string | The phone number for this contact point. |

### Institution

Institutions are defined as nested objects with a standard identifier.

| Field Name | Type | Description |
| ---------- | ---- | ----------- |
| identifier | string | The unique identifier for this institution.  Must be one of the following: "byu", "byui", "byuh", "ldsbc", "law" |
| name | string | A human-readable name for this institution ("Brigham Young University", "BYU-Idaho", etc).  Unlike identifier, these are not strictly enumerated values, but values should be chosen and used consistently for each institution. |

### CclaRoles

There are certain roles that are broadly understood across all CCLA institutions, though it is left up to each site to define the precise criteria which qualifies a user for membership in a given role.
It is NOT intended that this list should be interpreted as an exhaustive list of all roles at all institutions, and we expect that individual institutions will define additional roles to meet unique needs (for example,
BYU often differentiates between undergraduate and graduate students, but this differentiation is not needed at other schools).  Such additional roles lie outside the scope of this specification, and no expectation
of support for those roles should be required by any cross-institutional software. In the event the person is deceased, there should be no roles present.

**Bold** role names indicate a broad category of which *italicized* roles are a subset.

| Value     | Description |
| --------- | ----------- |
| **alumni**    | A person having this role is someone who has been granted alum status by the institution. |
| **blacklisted** | A person having this role has been blacklisted at the institution. |
| **circAccess** | A person having this role is someone who can check out items at the institution. |
| *donorCircAccess* | A person having this role is a donor who can check out items at the institution. |
| *specialCircAccess* | A person having this role is somehow special and has been granted permission to check out items at the institution. |
| **databaseAccess** | A person having this role is someone who has been granted access to databases at the insitution. |
| *donorDatabaseAccess* | A person having this role is a donor who has been granted access to databases at the institution. |
| *specialDatabaseAccess* | A person having this role is somehow special and has been granted access to database at the institution. |
| **dependentOfEmployee** | A person having this role can be claimed by an employee as a dependent. |
| *dependentOfLibraryEmployee* | A person having this role can be claimed by a library employee as a dependent. |
| **donor** | A person having this role is a donor at the institution. |
| **employee**  | A person having this role is a current employee at the institution. |
| *adminEmployee* | A person having this role is a current admin employee at the institution. |
| *facultyEmployee* | A person having this role is a current member of the faculty at the institution. |
| *staffEmployee* | A person having this role is a current staff employee at the institution. |
| *teachingEmployee* | A person having this role is a current teaching employee at the institution. |
| **formerStudent** | A person having this role was formerly an active student at the university. |
| **libraryAccountBlocked** | A person having this role has had their library account blocked at the institution. |
| **libraryEmployee** | A person having this role is a current employee in the library at the institution. |
| *adminLibraryEmployee* | A person having this role is a current admin employee in the library at the institution. |
| *facultyLibraryEmployee* | A person having this role is a current member of the faculty in the library at the institution. |
| *staffLibraryEmployee* | A person having this role is a current staff employee in the library at the institution. |
| *studentLibraryEmployee* | A person having this role is a current student employee in the library at the instution. |
| **nursing** | A person having this role is currently affiliated with the nursing program at the institution. |
| *nursingFaculty* | A person having this role is currently a faculty memberin the nursing program at the institution. |
| *nursingStudent* | A person having this role is currently enrolled as a student in the nursing program at the institution. |
| **retired** | A person having this role has been granted retired status by the institution. |
| *adminRetired* | A person having this role is a former admin employee who has been granted retired status by the institution. |
| *facultyRetired* | A person having this role is a member of the faculty who has been granted retired status by the institution. |
| *staffRetired* | A person having this role is a former staff employee who has been granted retired status by the institution. |
| **spouse** | A person having this role is the spouse of a person who has been granted privileges by the institution. |
| *spouseOfEmployee* | A person having this role is the spouse of a person who has employee status at the institution. |
| *spouseOfRetired* | A person having this role is the spouse of a person who has been granted retired status by the institution. |
| *spouseOfStudent* | A person having this role is the spouse of a person who as student status at the intitution. |
| *survivingSpouse* | A person having this role is the surviving spouse of a person who was granted privileges at the institution. |
| **student**   | A person having this role is currently an active student at the institution. |
| *enrolledStudent* | A person having this role is currently enrolled in classes at the institution. | 
| *graduateStudent* | A person having this role is currently enrolled as a graduate student at the institution. |
| *onlineStudent* | A person having this role is currently enrolled in online classes at the institution. |
| *undergraduateStudent* | A person having this role is currently enrolled as an undergraduate student at the institution. |
| **volunteer** | A person having this role is a volunteer at the institution. |


### Example Record

The following document shows an example user record as it might appear in compliance with this specification:

```json
{
  "@type": "CclaUser",
  "identifier": "byu:dduck",
  "memberOf": {
    "@type": "Institution",
	"identifier": "byu",
	"name": "Brigham Young University"
  },
  "userId": "123863422",
  "username": "dduck",
  "name": "Donald Duck",
  "familyName/surname": "Duck",
  "familyNamePosition/surnamePosition": "last",
  "givenNames": "Donald Fauntleroy",
  "suffix": null,
  "preferredName": "Donald",
  "personalContact": {
    "@type": "ContactPoint",
    "address": "123 E 500 N, Provo UT  84604",
	"email": "donald.duck@gmail.com",
	"phone": "8015551234"
  },
  "workContact": {
    "@type": "ContactPoint",
    "address": "BYU Duckpond, Provo UT  84602",
	"email": "donald_duck@byu.edu",
	"phone": "8014223825"
  },
  "roles": [
    "student",
	"alumni",
	"employee",
	"faculty"
  ]
}
```

## RESTful APIs

Each institution should provide an RESTful API for accessing person information.  Due to the sensitive nature of
personal information, this service MUST be secured using OAUTH 2.  Information should be provided as a JSON-LD document.

Only one endpoint is required by specification, which references the person by use of their `identifier` as the resource ID (in the final path fragment of the URL), which should be the concatentation of institution `identifier` and the user's `username`.
For example, for a BYU user having a username "dduck", the following URL would satisfy this specification: `https://api.lib.byu.edu/CclaUser/byu:dduck`.

Additional endpoints and API variations may be defined by each institution, but (as with non-standard roles) institutions are not required to support custom APIs provided by other institutions.

## Error messages

Each implementation of this specification should support several error messages.

#### 404 Not Found

In the event the caller requests a user that does not exist, the service should return an http 404 status.

#### 500 Internal Server Error

In the event the caller requests a user that does exists but cannot be properly constructed, the service should return an http 500 status.